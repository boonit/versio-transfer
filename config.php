<?php
//Versio API settings
$username = ''; //email adres
$password = '';
$endpoint = '.nl'; // change to the correct versio.xx tld
$testmodus = 'false'; // change this if you want to use the API in sandbox mode, no real transactions will be made if in sandbox mode

//Domain options
$bestand = 'domeinlist.csv'; //csv file must contain header: domein,auth
$delimiter = ',';

$years = '1';
$contact_id = ''; //Houder ID die gebruikt moet worden voor de registratie/transfer. Parameter is verplicht
$dnstemplate_id = '';

$ns1 = '';
$ns2 = '';
$ns3 = '';